//
//  LoginViewController.swift
//  Instagram
//
//  Created by Harsh Sutariya on 04/03/23.
//

import UIKit

class LoginViewController: UIViewController {

    private let userNameEmailField: UITextField = {
        let textField = UITextField()
        return textField
    }()

    private let passwordField: UITextField = {
        let textField = UITextField()
        textField.isSecureTextEntry = true
        return textField
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    private let termsButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    private let privacyButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    private let createAccountButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    private let headerView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        view.backgroundColor = .systemBackground
    }
    
    private func addSubviews() {
        view.addSubview(userNameEmailField)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
        view.addSubview(termsButton)
        view.addSubview(privacyButton)
        view.addSubview(createAccountButton)
        view.addSubview(headerView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // assign frames
    }
    
    @objc private func didTapLoginButton() {}
    @objc private func didTapTermsButton() {}
    @objc private func didTapPrivacyButton() {}
    @objc private func didTapCreateAccountButton() {}
}
